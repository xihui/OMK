import com.github.javacliparser.FileOption;
import com.yahoo.labs.samoa.instances.Prediction;
import com.yahoo.labs.samoa.instances.*;
import moa.classifiers.MultiLabelLearner;
import moa.core.InstanceExample;
import moa.core.Measurement;
import moa.core.SizeOf;
import moa.core.TimingUtils;
import moa.evaluation.BasicMultiLabelPerformanceEvaluator;
import moa.evaluation.LearningEvaluation;
import moa.evaluation.preview.LearningCurve;
import moa.streams.ConceptDriftStream;
import moa.streams.MultiTargetArffFileStream;
import moa.streams.generators.RandomRBFGenerator;
import moa.streams.generators.cd.GradualChangeGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

public class PrequentialTest {

    public FileOption dumpFileOption = new FileOption("dumpFile", 'd', "File to append intermediate csv results to.", (String)null, "csv", true);

    private int totalInstances;

    public PrequentialTest(int amountInstances) {

        if (amountInstances == -1) {
            this.totalInstances = 2147483647;
        } else {
            this.totalInstances = amountInstances;
        }
    }



    public void run() throws IOException {

        Instance kernel = null;


        // preparation
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/20NG/20NG-F.arff", "20");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Bookmarks/bookmarks.arff", "-208");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Corel16k/Corel16k001.arff", "-153");
        MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Enron.arff", "53");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Mediamill/mediamill.arff", "-101");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Ohsumed/OHSUMED-F.arff", "23");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/tmc2007-500/tmc2007-500.arff", "22");


        stream.prepareForUse();

        
        Online_KM learner = new Online_KM();
        //MultiLabelLearner learner = BaseLine.getLearner("MLHT", stream.getHeader());
        //MLHT
        //MLSAMPkNN learner = new MLSAMPkNN();

        learner.setModelContext(stream.getHeader());
        learner.prepareForUse();

        PrequentialMultiLabelPerformanceEvaluator evaluator = new PrequentialMultiLabelPerformanceEvaluator();

        // Online process
        long starttime = TimingUtils.getNanoCPUTimeOfCurrentThread();
        int numberInstances = 0;

        while(numberInstances < totalInstances && stream.hasMoreInstances()) {

            InstanceExample trainInst = (InstanceExample) stream.nextInstance();

            Prediction prediction = learner.getPredictionForInstance(trainInst);
            evaluator.addResult(trainInst, prediction);
            learner.trainOnInstance(trainInst);

            ++numberInstances;

        }



        System.out.println(numberInstances + " instances.");
        long endtime = TimingUtils.getNanoCPUTimeOfCurrentThread();

        for(int i = 0; i < evaluator.getPerformanceMeasurements().length; i++)
            System.out.println(evaluator.getPerformanceMeasurements()[i].getName() + "\t" + String.format("%.3f",evaluator.getPerformanceMeasurements()[i].getValue()));

        String timeString = "Time: " + TimingUtils.nanoTimeToSeconds((endtime - starttime)) + " s  \n";
        System.out.println(timeString + "\n");

        /*
        ArrayList<Integer> countsPredict = learner.countsInsertList(true);
        ArrayList<Integer> countsInsert = learner.countsInsertList(false);
        ArrayList<Instance> centroides = learner.kernelList();

        
        for(int i = 0; i < countsInsert.size(); i++){
            double[] inst_labels = learner.instanceLabels(centroides.get(i));
            ArrayList<Double> labels  = new ArrayList<Double>();
            for(int j = 0; j < inst_labels.length; j++){
                labels.add(inst_labels[j]);
            }
            System.out.println("instance" + i + " : " + labels.toString());
            System.out.println("countsInsert : " + countsInsert.get(i));
            System.out.println("countsPredict : " + countsPredict.get(i));
            System.out.println("   ");

        }
        */

        /*
        ArrayList<Instance> centroides = learner.kernelList();
        for(int i = 0; i < centroides.size(); i++){
            double[] inst_labels = learner.instanceAttributes(centroides.get(i));
            ArrayList<Double> labels  = new ArrayList<Double>();
            for(int j = 0; j < inst_labels.length; j++){
                labels.add(inst_labels[j]);
            }
            System.out.println("instance" + i + " : " + labels.toString());
        }

         */




        //BaseLine.executeMethod("Baye_com", "bookmarks",learner, starttime, evaluator);
        //System.out.println("Size of the model: " + SizeOf.fullSizeOf(learner) + " bytes\n");


    }

    public static void main(String[] args) throws IOException {
       PrequentialTest batch = new PrequentialTest(-1);
        batch.run();
    }

}







