import com.github.javacliparser.FileOption;
import com.github.javacliparser.IntOption;
import com.yahoo.labs.samoa.instances.Prediction;
import com.yahoo.labs.samoa.instances.*;
import moa.classifiers.MultiLabelLearner;
import moa.classifiers.multilabel.meta.OzaBagAdwinML;
import moa.classifiers.multilabel.trees.ISOUPTree;
import moa.core.InstanceExample;
import moa.core.Measurement;
import moa.core.SizeOf;
import moa.core.TimingUtils;
import moa.evaluation.BasicMultiLabelPerformanceEvaluator;
import moa.evaluation.LearningEvaluation;
import moa.evaluation.preview.LearningCurve;
import moa.streams.ConceptDriftStream;
import moa.streams.MultiTargetArffFileStream;
import moa.streams.generators.RandomRBFGenerator;
import moa.streams.generators.cd.GradualChangeGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.Object.*;
import java.util.ArrayList;

public class PrequentialTest2 {

    public FileOption dumpFileOption = new FileOption("dumpFile", 'd', "File to append intermediate csv results to.", (String)null, "csv", true);
    public IntOption sampleFrequencyOption = new IntOption("sampleFrequency", 'f', "How many instances between samples of the learning performance.", 10000, 0, 2147483647);


    private int totalInstances;

    public PrequentialTest2(int amountInstances) {

        if (amountInstances == -1) {
            this.totalInstances = 2147483647;
        } else {
            this.totalInstances = amountInstances;
        }
    }

    public void run() throws IOException {

        File dumpFile = this.dumpFileOption.getFile();
        PrintStream immediateResultStream = null;
        boolean firstDump = true;
        if (dumpFile != null) {
            try {
                if (dumpFile.exists()) {
                    immediateResultStream = new PrintStream(new FileOutputStream(dumpFile, true), true);
                } else {
                    immediateResultStream = new PrintStream(new FileOutputStream(dumpFile), true);
                }
            } catch (Exception var36) {
                throw new RuntimeException("Unable to open immediate result file: " + dumpFile, var36);
            }
        }
        LearningCurve learningCurve = new LearningCurve("learning evaluation instances");

        // preparation
        MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/20NG/20NG-F.arff", "20");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Bookmarks/bookmarks.arff", "-208");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Corel16k/Corel16k001.arff", "-153");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Enron.arff", "53");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Mediamill/mediamill.arff", "-101");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/Ohsumed/OHSUMED-F.arff", "23");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/prequential/tmc2007-500/tmc2007-500.arff", "22");




        /*
        MetaMultilabelGenerator stream1 = new MetaMultilabelGenerator();
        stream1.binaryGeneratorOption.setValueViaCLIString("generators.RandomTreeGenerator");
        stream1.labelCardinalityOption.setValue(1.6);
        stream1.numLabelsOption.setValue(10);
        MetaMultilabelGenerator stream2 = new MetaMultilabelGenerator();
        stream2.binaryGeneratorOption.setValueViaCLIString("generators.RandomTreeGenerator");
        stream2.labelCardinalityOption.setValue(1.6);
        stream2.labelDependencyChangeRatioOption.setValue(0.6);
        stream2.numLabelsOption.setValue(10);
        ConceptDriftStream stream3 = new ConceptDriftStream();
        stream3.streamOption.setCurrentObject(stream1);
        stream3.driftstreamOption.setCurrentObject(stream2);
        stream3.positionOption.setValue(200000);
        MetaMultilabelGenerator stream4 = new MetaMultilabelGenerator();
        stream4.binaryGeneratorOption.setValueViaCLIString("generators.RandomTreeGenerator");
        stream4.labelCardinalityOption.setValue(1.6);
        stream4.labelDependencyChangeRatioOption.setValue(1);
        stream4.numLabelsOption.setValue(10);
        ConceptDriftStream stream5 = new ConceptDriftStream();
        stream5.streamOption.setCurrentObject(stream3);
        stream5.driftstreamOption.setCurrentObject(stream4);
        stream5.positionOption.setValue(500000);
        stream5.widthOption.setValue(100000);
        ConceptDriftStream stream = new ConceptDriftStream();
        stream.streamOption.setCurrentObject(stream5);
        stream.driftstreamOption.setCurrentObject(stream1);
        stream.positionOption.setValue(800000);
        //1.5->3.5, 2.8
        //1.8->3, 1.6

         */





        stream.prepareForUse();

        //Online_KM learner = new Online_KM();
        //MLSAMPkNN learner = new MLSAMPkNN();
        MultiLabelLearner learner = BaseLine.getLearner("MLHT", stream.getHeader());
        //EaMLHT
        //EaISOUPTree
        //EaCC
        //EaPS
        //EaBR

        learner.setModelContext(stream.getHeader());
        learner.prepareForUse();

        PrequentialMultiLabelPerformanceEvaluator evaluator = new PrequentialMultiLabelPerformanceEvaluator();
        //evaluator.alphaOption.setValue(1.00);

        // Online process
        long starttime = TimingUtils.getNanoCPUTimeOfCurrentThread();
        int numberInstances = 0;
        while(numberInstances < totalInstances && stream.hasMoreInstances()) {

            InstanceExample trainInst = (InstanceExample) stream.nextInstance();
            Prediction prediction = learner.getPredictionForInstance(trainInst);
            evaluator.addResult(trainInst, prediction);
            learner.trainOnInstance(trainInst);

            if (numberInstances % (long)this.sampleFrequencyOption.getValue() == 0L || !stream.hasMoreInstances()) {
                learningCurve.insertEntry(new LearningEvaluation(new Measurement[]{new Measurement("learning evaluation instances", (double)numberInstances)}, evaluator, learner));
                if (immediateResultStream != null) {
                    if (firstDump) {
                        immediateResultStream.println(learningCurve.headerToString());
                        firstDump = false;
                    }

                    immediateResultStream.println(learningCurve.entryToString(learningCurve.numEntries() - 1));
                    immediateResultStream.flush();
                }
                //evaluator.reset();
            }

            ++numberInstances;

        }

        if (immediateResultStream != null) {
            immediateResultStream.close();
        }



        System.out.println(numberInstances + " instances.");
        long endtime = TimingUtils.getNanoCPUTimeOfCurrentThread();

        for(int i = 0; i < evaluator.getPerformanceMeasurements().length; i++)
            System.out.println(evaluator.getPerformanceMeasurements()[i].getName() + "\t" + String.format("%.3f",evaluator.getPerformanceMeasurements()[i].getValue()));

        String timeString = "Time: " + TimingUtils.nanoTimeToSeconds((endtime - starttime)) + " s  \n";
        System.out.println(timeString + "\n");


        /*
        ArrayList<Instance> utilise = learner.getutilise();
        for(int i = 0; i < utilise.size(); i++)
            System.out.println("cluster" + i + "  :"+ utilise.get(i));
        */


        //BaseLine.executeMethod("MLSAMPkNN", "bookmarks",learner, starttime, evaluator);
        //System.out.println("Size of the model: " + SizeOf.fullSizeOf(learner) + " bytes\n");


    }

    public static void main(String[] args) throws IOException {
       PrequentialTest2 batch = new PrequentialTest2(-1);
       batch.dumpFileOption.setValue("./out/results/stationnary/20NG/MLHT.csv");
       batch.run();
    }

}







