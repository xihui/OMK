import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import static java.lang.Math.sqrt;

public class RandomProjection {

    private double[][] projection;



    public RandomProjection (int n, int p) {
        if (n < 2) {
            throw new IllegalArgumentException("Invalid dimension of input space: " + n);
        }

        if (p < 1 || p > n) {
            throw new IllegalArgumentException("Invalid dimension of feature space: " + p);
        }

        double[][] projection = new double[p][n];
        Random ran = new Random();
        for (int i = 0; i < p; i++) {
            for (int j = 0; j < n; j++) {
                projection[i][j] = ran.nextGaussian();
            }
        }

        // Make the columns of the projection matrix orthogonal
        // by modified Gram-Schmidt algorithm.
        unitize(projection[0]);
        for (int i = 1; i < p; i++) {
            for (int j = 0; j < i; j++) {
                double t = dot(projection[i], projection[j]);
                projection[i] = axpy(t, projection[j], projection[i]);
            }
            unitize(projection[i]);
        }

        this.projection = projection;
    }

    public RandomProjection() {

    }

    public double[][] getprojection() {
        return this.projection;
    }

    public static double norm(double[] x) {
        double norm = 0.0;

        for (double n : x) {
            norm += n * n;
        }

        norm = sqrt(norm);

        return norm;
    }

    public static void unitize(double[] x) {
        double n = norm(x);

        for (int i = 0; i < x.length; i++) {
            x[i] /= n;
        }
    }

    /**
     * Returns the dot product between two vectors.
     * @param x a vector.
     * @param y a vector.
     * @return the dot product.
     */
    public double dot(double[] x, double[] y) {
        if (x.length != y.length) {
            throw new IllegalArgumentException("Arrays have different length.");
        }

        double sum = 0.0;
        for (int i = 0; i < x.length; i++) {
            sum += x[i] * y[i];
        }

        return sum;
    }

    public double[] dotMatrix(double[] x) {
        if (x.length != projection[0].length) {
            throw new IllegalArgumentException("Arrays have different length."+ projection[0].length + "  x "+  x.length);
        }
        double[] newX = new double[projection.length];

        for (int i = 0; i < projection.length; i++) {
            newX[i] = dot(x, projection[i]);
        }

        return newX;
    }


    /**
     * Update an array by adding a multiple of another array y = a * x + y.
     * @param a the scale factor.
     * @param x a vector.
     * @param y the input and output vector.
     * @return the vector y.
     */
    public static double[] axpy(double a, double[] x, double[] y) {
        if (x.length != y.length) {
            throw new IllegalArgumentException(String.format("Arrays have different length: x[%d], y[%d]", x.length, y.length));
        }

        for (int i = 0; i < x.length; i++) {
            y[i] += a * x[i];
        }

        return y;
    }


    public static void main(String[] args) {
        RandomProjection matrix = new RandomProjection( 5, 3 );
        double[][] projection = matrix.getprojection();
        for (int i = 0; i< projection.length; i++) {
            System.out.println("vecteur" + i + " : " +  Arrays.toString(projection[i]));
        }
    }
}
