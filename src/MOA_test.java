import com.yahoo.labs.samoa.instances.*;
import moa.core.InstanceExample;
import moa.core.SizeOf;
import moa.core.TimingUtils;
import moa.options.ClassOption;
import moa.streams.ConceptDriftStream;
import moa.streams.MultiTargetArffFileStream;
import moa.streams.generators.*;
import moa.tasks.TaskMonitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;


public class MOA_test {

    private int totalInstances;

    public MOA_test(int amountInstances) {

        if (amountInstances == -1) {
            this.totalInstances = 2147483647;
        } else {
            this.totalInstances = amountInstances;
        }
    }

    public void run() throws IOException {

        // preparation
        MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/20NG/20NG-F.arff", "20");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Bookmarks/bookmarks.arff", "-208");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Corel16k/Corel16k001.arff", "-153");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Enron.arff", "53");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Mediamill/mediamill.arff", "-101");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Ohsumed/OHSUMED-F.arff", "23");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/tmc2007-500/tmc2007-500.arff", "22");
        //MultiTargetArffFileStream stream = new MultiTargetArffFileStream(".././MLS_Datasets/Yeast.arff", "14");

        /*
        MetaMultilabelGenerator stream = new MetaMultilabelGenerator();
        stream.binaryGeneratorOption.setValueViaCLIString("generators.RandomRBFGeneratorDrift");
        //stream.labelDependencyChangeRatioOption.setValue(0.5);
        stream.numLabelsOption.setValue(10);
        */


        stream.prepareForUse();

        Online_KM learner = new Online_KM();
        learner.setModelContext(stream.getHeader());
        learner.prepareForUse();
        learner.resetLearningImpl();


        PrequentialMultiLabelPerformanceEvaluator evaluator = new PrequentialMultiLabelPerformanceEvaluator();

        // Online process
        long starttime = TimingUtils.getNanoCPUTimeOfCurrentThread();
        int numberInstances = 0;
        while(numberInstances < totalInstances && stream.hasMoreInstances()) {

            InstanceExample trainInst = stream.nextInstance();

            Prediction prediction = learner.getPredictionForInstance(trainInst);
            evaluator.addResult(trainInst, prediction);
            learner.trainOnInstance(trainInst);


            ++numberInstances;;

        }

        //ArrayList<Double> errors = learner.geterrors();
        System.out.println(numberInstances + " instances.");

        /*
        for(int i=0; i< errors.size(); i++)
            System.out.println("kernel "+i + " has errors" + String.format("%.2f",errors.get(i)));

         */

        long endtime = TimingUtils.getNanoCPUTimeOfCurrentThread();

        for(int i = 0; i < evaluator.getPerformanceMeasurements().length; i++)
            System.out.println(evaluator.getPerformanceMeasurements()[i].getName() + "\t" + String.format("%.3f",evaluator.getPerformanceMeasurements()[i].getValue()));

        String timeString = "Time: " + TimingUtils.nanoTimeToSeconds((endtime - starttime)) + " s  \n";
        System.out.println(timeString + "\n");

    }

    public static void main(String[] args) throws IOException {
        MOA_test batch = new MOA_test(-1);
        batch.run();
    }

}

/*

 */





